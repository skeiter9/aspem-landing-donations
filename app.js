import angular from 'angular';
import app from './app/boot/app.js';

{

  window.addEventListener('DOMContentLoaded', () => {
    window.app = angular
      .bootstrap(document.body, [app.name], {strictDi: true});
  });
}
