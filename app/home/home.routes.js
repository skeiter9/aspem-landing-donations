export const homeRules = ($uRP) => {

  $uRP

  .rule(($injector, $location) => {
    const path = $location.path();
    const normalized = $location.path().toLowerCase();
    if ($location.path() !== normalized) return normalized;
  })

  .rule(($injector, $location) => {
    let path = $location.path();
    if (/\s/.test(path)) return path.replace(/\s/g, '-');
  });

};

export const homeRoutes = ($sP) => {

  $sP

    .state('main-home', {
      parent: 'layout',
      url: '/',
      resolve: {
        loadTranslattions: ['$translatePartialLoader', '$translate', ($tPL, $tr) => {
          $tPL.addPart('home');
          return $tr.refresh();
        }]
      },
      views: {
        content: {
          template: require('./templates/base.jade')(),
          controllerAs: 'vm',
          controller: ['layout', '$rootScope', function(l, $rS) {
            this.changeLanguage = lang => l.changeLanguage(lang);
          }]
        }
      }
    })

};
