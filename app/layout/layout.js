import angular from 'angular';

if (__ENV__ !== 'test') {
  require('./assets/fonts/icomoon/style.css');
  //require('./styles/swiper.css');
}

import styles from "./styles/layout.css";

require('angular-translate');
require('angular-translate-loader-partial');

export default angular.module('wagonlayout', [
  require('angular-animate'),
  require('ng-fx'),
  'pascalprecht.translate'
])

  .config(['$translateProvider', function($tP) {
    $tP
      .useSanitizeValueStrategy('escape')
      .useLoader('$translatePartialLoader', {
        urlTemplate: '/i18n/{part}-{lang}.json'
      })
      .preferredLanguage('es');
  }])

  .run(['$$rAF', ($$rAF) => {
    //resize event optimized through requestAnimationFrame
    const throttle = (type, name, obj = window) => {

      let running = false;

      const func = () => {
        if (running) return;
        running = true;
        $$rAF(() => {
          running = false;
          obj.dispatchEvent(new CustomEvent(name));
        });
      };

      obj.addEventListener(type, func);
    };

    throttle('resize', 'optimizedResize');

  }])

  .service('layout', ['$translate', function($tr) {
    this.title = 'Ettopersa';
    this.changeLanguage = lang => $tr.refresh()
      .then(() => $tr.use(lang));
    this.launchSwiper = () => {
      const Swiper = require('swiper');
      const swiper = new Swiper('.swiper-container', {
          nextButton: '.swiper-button-next',
          prevButton: '.swiper-button-prev',
          pagination: '.swiper-pagination',
          paginationClickable: true,
          autoplay: 5000
      });
    };

  }])

  .controller('LayoutController', ['layout', '$scope', function(l, $s) {
    this.data = l;
    if (window.innerWidth > 1024) $s.toggleMenuStatus = true;
    const Swiper = require('swiper');
    const swiper = new Swiper('.swiper-container', {
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        pagination: '.swiper-pagination',
        paginationClickable: true
    });
  }]);
