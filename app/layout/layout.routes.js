export const layoutRoutes = ($sP) => {

  $sP
    .state('layout', {
      abstract: true,
      resolve: {
        loadTranslattions: ['$translatePartialLoader', '$translate', ($tPL, $tr) => {
          $tPL.addPart('base');
          return $tr.refresh();
        }]
      },
      template: require('./templates/layout.jade')(),
      controller: 'LayoutController',
      controllerAs: 'layout'
    });

};
